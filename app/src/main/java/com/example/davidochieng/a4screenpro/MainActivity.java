package com.example.davidochieng.a4screenpro;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.content.Intent;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

      Button toScreen2,toScreen3,toScreen4;

        toScreen2=(Button)findViewById(R.id.toScreen2);
        toScreen2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intent=new Intent(getApplicationContext(), SecondActivity.class);
                startActivity(intent);
            }
        });

    /////////////////////////////////////////////=================================

        toScreen3=(Button)findViewById(R.id.toScreen3);
        toScreen3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), ThirdActivity.class);
                startActivity(intent);
            }
        });

    /////////////////////////////////////////////==================================


        toScreen4=(Button)findViewById(R.id.toScreen4);
        toScreen4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), ForthActivity.class);
                startActivity(intent);
            }
        });
    }}
